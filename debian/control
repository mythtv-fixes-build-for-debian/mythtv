Source: mythtv-snouf
Section: graphics
Priority: optional
Maintainer: Jonas Fourquier <snouf@mythtv-fr.org>
Bugs: http://mythtv-fr.org/bugtracker/
Homepage: http://www.mythtv-fr.org/
X-Python-Version: >= 2.7
Standards-Version: 3.9.7
Build-Conflicts: libmyth-dev, liboss4-salsa-dev, libroar-dev, qtbase5-dev-tools
Build-Depends: debhelper (>= 9),libmp3lame-dev, libxext-dev, libvorbis-dev,
 libmysqlclient-dev, libfreetype6-dev, libjack-jackd2-dev,
 libavc1394-dev [linux-any], libfaad-dev, liblircclient-dev, libsamplerate0-dev,
 libasound2-dev [linux-any], libxinerama-dev, libdts-dev, quilt, libtag1-dev,
 libiec61883-dev (>= 1.0.0) [linux-any], libxxf86vm-dev, po-debconf,
 libxvmc-dev, libdvdnav-dev, texi2html, ccache, libssl-dev, libcec-dev | libcec1-dev,
 yasm [any-amd64 any-i386], libxrandr-dev, libfftw3-dev, dh-autoreconf,
 libfaac-dev, libfribidi-dev, libxml2-dev, libgl1-mesa-dev | libgl-dev,
 liba52-0.7.4-dev, libxvidcore-dev, libpulse-dev, libtool-bin,
 libvdpau-dev [any-amd64 any-i386], libqt4-opengl-dev (>= 4.5), libdirectfb-dev,
 distcc [mipsel mips], uuid-dev, libva-dev [any-amd64 any-i386],
 libqtwebkit-dev, libx264-dev (>= 2:0.142~), libvpx-dev (>= 1.3.0),
 libass-dev (>= 0.10.2), libavahi-compat-libdnssd-dev, libsdl1.2-dev,
 libcrystalhd-dev [any-amd64 i386], libqjson-dev, libhdhomerun-dev,
 python | python-all | python-dev | python-all-dev, librtmp-dev,
 gnutls-dev | libgnutls28-dev

Package: mythtv
Architecture: all
Pre-Depends: mysql-server | virtual-mysql-server
Depends: mythtv-database (>= ${source:Version}), mythtv-frontend (>= ${source:Version}), mythtv-backend (>= ${source:Version}), ${misc:Depends}
Recommends: mythtv-doc
Description: Personal video recorder application (client and server)
 MythTV implements the following PVR features, and more, with a
 unified graphical interface:
 .
  - Basic 'live-tv' functionality. Pause/Fast Forward/Rewind "live" TV.
  - Video compression using RTjpeg or MPEG-4
  - Program listing retrieval using XMLTV
  - Themable, semi-transparent on-screen display
  - Electronic program guide
  - Scheduled recording of TV programs
  - Resolution of conflicts between scheduled recordings
  - Basic video editing
 .
 This package will install a complete MythTV client/server environment on a
 single system, including a MySQL server.
 .
 The powerpc packages were compiled with altivec and sparc packages with
 ultra-sparc support.

Package: mythtv-common
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python:Depends}, libxml-xpath-perl, mysql-client | virtual-mysql-client, ttf-freefont, fonts-liberation, ttf-tiresias, fonts-droid-fallback | fonts-droid
Recommends: mythtv-doc (>= ${source:Version}), python-selinux
Conflicts: mythtv (<< 0.8-1), mythvideo
Replaces: mythtv (<< 0.8-1), mythtv-frontend (<= 0.26.0), mythtv-backend (<= 0.20.2.svn20071021-0.0), mythmusic (<= 0.20.2.svn20080126-0.0), mythtv-themes (<= 0.21.svn20090824-0.4), mythcontrols (<< 0.22-0.5), mythtv-backend-python (<= 0.24~), libmyth-0.26-0 (<= 0.26.0)
Description: Personal video recorder application (common data)
 MythTV provides a unified graphical interface for recording and viewing
 television programs. Refer to the mythtv package for more information.
 .
 This package contains infrastructure needed by both the client and the
 server.

Package: mythtv-doc
Architecture: all
Conflicts: mythtv (<< 0.8-1)
Section: doc
Depends: ${misc:Depends}
Replaces: mythtv (<< 0.8-1)
Description: Personal video recorder application (documentation)
 MythTV provides a unified graphical interface for recording and viewing
 television programs. Refer to the mythtv package for more information.
 .
 This package contains documentation, including the MythTV HOWTO.

Package: mythtv-database
Architecture: all
Depends: mythtv-common (>= ${source:Version}), ${misc:Depends}, libdbd-mysql-perl, mysql-client | virtual-mysql-client, cron | cron-daemon
Recommends: mysql-server | virtual-mysql-server
Conflicts: mythtv (<< 0.8-1), mythtv-common (<< 0.8-2)
Replaces: mythtv (<< 0.8-1), mythtv-common (<< 0.8-2)
Description: Personal video recorder application (database)
 MythTV provides a unified graphical interface for recording and viewing
 television programs. Refer to the mythtv package for more information.
 .
 The powerpc packages were compiled with altivec and sparc packages with
 ultra-sparc support.
 .
 This package sets up a MySQL database for use MythTV. It should be
 installed on the system where the MySQL server resides.

Package: mythtv-backend
Architecture: any
Pre-Depends: dpkg (>= 1.15.7.2~)
Depends: mythtv-common (>= ${source:Version}), ${shlibs:Depends}, ${misc:Depends}, cron | cron-daemon, wget, libjs-jquery, mythlogserver, mythtv-transcode, python-pycurl, python-mythtv
Conflicts: mythtv (<< 0.8-1)
Replaces: mythtv (<< 0.8-1), mythtv-frontend (<= 0.20-0.4), mythtv-common (<= 0.27.0)
Recommends: mythtv-database, logrotate, xmltv-util
Suggests: mythtv-frontend, mythweb
Description: Personal video recorder application (server)
 MythTV provides a unified graphical interface for recording and viewing
 television programs. Refer to the mythtv package for more information.
 .
 This package contains only the server software, which provides video and
 audio capture and encoding services. In order to be useful, it requires a
 mythtv-frontend installation, either on the same system or one reachable
 via the network.
 .
 A database is also required. The mythtv-database package must be installed,
 either on the same system, or one reachable via the network.
 .
 The powerpc packages were compiled with altivec and sparc packages with
 ultra-sparc support.
 .
 For a complete installation of all MythTV components, install the 'mythtv'
 package.

Package: mythtv-frontend
Architecture: any
Depends: mythtv-common (>= ${source:Version}), ${shlibs:Depends}, ${misc:Depends}, ${python:Depends}, python-mythtv, mythlogserver, transcode
Suggests: mythtv-backend, mythmusic, mythweather, mythgallery, mythgame
Conflicts: mythtv (<< 0.8-1), mythstream (<< 0.20)
Replaces: mythtv (<< 0.8-1), mythtv-backend-python
Description: Personal video recorder application (client)
 MythTV provides a unified graphical interface for recording and viewing
 television programs. Refer to the mythtv package for more information.
 .
 This package contains only the client software, which provides a front-end
 for playback and configuration. It requires access to a mythtv-backend
 installation, either on the same system or one reachable via the network.
 .
 A database is also required. The mythtv-database package must be installed,
 either on the same system, or one reachable via the network.
 .
 The powerpc packages were compiled with altivec and sparc packages with
 ultra-sparc support.
 .
 For a complete installation of all MythTV components, install the 'mythtv'
 package.

Package: mythlogserver
Architecture: any
Depends: mythtv-common (>= ${source:Version}), ${shlibs:Depends}, ${misc:Depends}, ${python:Depends}, rsyslog | system-log-daemon
Description: MythTV log messages server
 Log messages generated by MythTV programs are sent to mythlogserver which
 distributes them based on the --syslog, and --logpath command line options
 as well as to the database (if --nodblog isn't set.) 

Package: libmyth-0.27-0
Architecture: any
Section: libs
#Multi-Arch: same
#Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}, libqt4-sql-mysql, procps, iputils-ping
Recommends: udisks2
Conflicts: mythtv (<< 0.7-5), libmyth-0.21
Replaces: mythtv (<< 0.7-5), libmyth-0.21, mythtv-frontend (<= 0.26.0-dmo7)
Description: Common library code for MythTV and add-on modules (runtime)
 MythTV provides a unified graphical interface for recording and viewing
 television programs. Refer to the mythtv package for more information.
 .
 The powerpc packages were compiled with altivec and sparc packages with
 ultra-sparc support.
 .
 This package contains a shared library, libmyth, which is used by various
 components in the system.

Package: libmyth-dev
Architecture: any
Section: libdevel
#Multi-Arch: same
Depends: libmyth-0.27-0 (= ${binary:Version}), ${misc:Depends}
Conflicts: libmyth-0.20-dev
Replaces: libmyth-0.20-dev
Description: Common library code for MythTV and add-on modules (development)
 MythTV provides a unified graphical interface for recording and viewing
 television programs. Refer to the mythtv package for more information.
 .
 This package contains files needed for developing applications which use
 libmyth (such as the various add-ons for MythTV)

Package: libmythtv-perl
Architecture: all
Section: perl
Replaces: mythtv-perl (<= 0.20.2.svn20071109-0.0)
Conflicts: mythtv-perl (<= 0.20.2.svn20071109-0.0)
Depends: ${perl:Depends}, ${misc:Depends}, libdbi-perl, libdbd-mysql-perl, libwww-perl, libnet-upnp-perl, libio-socket-inet6-perl
Description: Personal video recorder application (common data)
 MythTV provides a unified graphical interface for recording and viewing
 television programs. Refer to the mythtv package for more information.
 .
 This package contains Perl bindings to access mythtv datas.

Package: python-mythtv
Architecture: all
Section: python
Depends: ${python:Depends}, ${misc:Depends}, python-mysqldb, python-lxml
Description: Personal video recorder application (common data)
 MythTV provides a unified graphical interface for recording and viewing
 television programs. Refer to the mythtv package for more information.
 .
 This package contains Python bindings to access mythtv datas.

Package: php-mythtv
Architecture: all
Depends: ${misc:Depends}, libapache2-mod-php5 (>= 5.3) | php5 (>= 5.3)
Description: PHP Bindings for MythTV
 MythTV provides a unified graphical interface for recording and viewing
 television programs. Refer to the mythtv package for more information.
 .
 This package contains files needed for using PHP based applications that
 connect to MythTV backends.

Package: mythffmpeg
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Section: utils
Description: Special ffmpeg version for nuvexport
 Special ffmpeg version for nuvexport. This binary is linked against the
 libmythav* libraries and not libraries in libavc* packages.

Package: libmythavcodec54
Architecture: any
Section: libs
#Multi-Arch: same
#Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: libmyth-0.24-0 (<= 0.24-0.6)
Replaces: libmyth-0.24-0 (<= 0.24-0.6)
Description: libavcodec package for MythTV
 This package is only used by MythTV packages.

Package: libmythavdevice54
Architecture: any
Section: libs
#Multi-Arch: same
#Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: libmyth-0.24-0 (<= 0.24-0.6)
Replaces: libmyth-0.24-0 (<= 0.24-0.6)
Description: libavdevice package for MythTV
 This package is only used by MythTV packages.

Package: libmythavfilter3
Architecture: any
Section: libs
#Multi-Arch: same
#Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: libmyth-0.24-0 (<= 0.24-0.6)
Replaces: libmyth-0.24-0 (<= 0.24-0.6)
Description: libavfilter package for MythTV
 This package is only used by MythTV packages.

Package: libmythavformat54
Architecture: any
Section: libs
#Multi-Arch: same
#Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: libmyth-0.24-0 (<= 0.24-0.6)
Replaces: libmyth-0.24-0 (<= 0.24-0.6)
Description: libavformat package for MythTV
 This package is only used by MythTV packages.

Package: libmythavutil52
Architecture: any
Section: libs
#Multi-Arch: same
#Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: libmyth-0.24-0 (<= 0.24-0.6)
Replaces: libmyth-0.24-0 (<= 0.24-0.6)
Description: libavutil package for MythTV
 This package is only used by MythTV packages.

Package: libmythpostproc52
Architecture: any
Section: libs
#Multi-Arch: same
#Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: libmyth-0.24-0 (<= 0.24-0.6)
Replaces: libmyth-0.24-0 (<= 0.24-0.6)
Description: libpostproc package for MythTV
 This package is only used by MythTV packages.

Package: libmythswscale2
Architecture: any
Section: libs
#Multi-Arch: same
#Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: libmyth-0.24-0 (<= 0.24-0.6)
Replaces: libmyth-0.24-0 (<= 0.24-0.6)
Description: libswscale package for MythTV
 This package is only used by MythTV packages.

Package: libmythswresample0
Architecture: any
Section: libs
#Multi-Arch: same
#Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: swresample package for MythTV
 This package is only used by MythTV packages.

Package: libmythzmq1
Architecture: any
Section: libs
#Multi-Arch: same
#Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Replaces: libmyth-0.26-0
Conflicts: libmyth-0.26-0
Description: Lightweight messaging kernel package for MythTV
 This package is only used by MythTV packages.

Package: mythtv-perl
Architecture: all
Depends: libmythtv-perl, ${misc:Depends}
Section: perl
Description: Dummy package to install libmythtv-perl
 This package can be removed.

Package: mythtv-transcode
Architecture: any
Depends: mythtv-common, ${shlibs:Depends}, ${misc:Depends}
Conflicts: mythtv-backend (<= 0.26.0-dmo12)
Replaces: mythtv-backend (<= 0.26.0-dmo12) 
Suggests: mythtv-backend, mytharchive
Description: Utilities used for transcoding MythTV tasks
 Some utilities are applicable for both a frontend or a backend machine.
 This package provides utilities that can be used on both without
 requiring an entire backend to be installed.
